package com.ubion.test.dao;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("TestDao")
public class TestDao {

	private static final Logger logger = LoggerFactory.getLogger(TestDao.class);
	
	@Autowired
	protected SqlSession sqlSession;
	
	public Map<String, String> test(int id) {
		logger.debug("param ==> " + id);
		
		return sqlSession.selectOne("test.getTest", id);
	}
}
