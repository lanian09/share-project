package com.ubion.test.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ubion.test.dao.TestDao;

@RestController
@RequestMapping("/test")
public class TestController {

	private static final Logger logger = LoggerFactory.getLogger(TestController.class);
	
	@Resource(name="TestDao")
	private TestDao testDao;
	
	//NOTED @RequestMapping(value = "/{id}", method = RequestMethod.GET,headers="Accept=application/json")
	@RequestMapping(value="/test")
	public String test() {
		
		logger.debug("(default)");
		return "test";
	}
	
	@RequestMapping(value="/test/{id}")
	public Map<String, String> testID(@PathVariable int id) {
		logger.debug("param ==> " + id);
		
		Map<String, String> map = testDao.test(id); 
		
		return map;
	}
}
